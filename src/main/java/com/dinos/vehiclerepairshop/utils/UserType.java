package com.dinos.vehiclerepairshop.utils;

public enum UserType {
    USER,
    ADMINISTRATOR;
}
