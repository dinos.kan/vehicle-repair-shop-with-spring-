package com.dinos.vehiclerepairshop.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static String username = "postgres";
    private static String password = "postgres";
    private static String dbUrl = "jdbc:postgresql://localhost:5432/vehicle_repair_shop";

    public Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(dbUrl, username, password);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

}
