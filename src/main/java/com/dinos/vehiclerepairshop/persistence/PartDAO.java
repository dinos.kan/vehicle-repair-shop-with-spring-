package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.Part;;

import java.util.List;

public interface PartDAO {
    void insertPart(Part part);

    boolean deletePartFromList(long partId);

    boolean updatePart(Part partToUpdate);

    List<Part> findAllParts();

    Part findPartById(long partId);

    List<Part> findPartByType(String partType);

    List<Part> findPartByCost(int partCost);

    List<Part> findRepairParts(long repairId);

    double repairPartsCost(long repairId);
}
