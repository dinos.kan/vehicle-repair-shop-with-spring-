package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.Vehicle;

import java.time.LocalDate;
import java.util.List;

public interface VehicleDAO {
    void insertVehicle(Vehicle vehicle);

    boolean deleteVehicleFromList(long id);

    boolean updateVehicle(Vehicle vehicleToUpdate);

    List<Vehicle> findAllVehicles();

    Vehicle findVehicleById(long vehicleId);

    Vehicle findVehicleByPlateNumber(String plateNumber);

    List<Vehicle> findVehiclesByUserId(long userId);
}
