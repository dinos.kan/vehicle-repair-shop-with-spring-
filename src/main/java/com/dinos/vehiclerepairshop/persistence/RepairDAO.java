package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.utils.RepairStatus;

import java.time.LocalDate;
import java.util.List;

public interface RepairDAO {
    void insertRepair(Repair repair);

    boolean deleteRepairFromList(long repairId);

    boolean updateRepair(Repair repairToUpdate);

    List<Repair> findAllRepairs();

    Repair findRepairById(long repairId);

    List<Repair> findVehicleRepairs(long vehicleId);

    double calcRepairCost(long repairId);
}
