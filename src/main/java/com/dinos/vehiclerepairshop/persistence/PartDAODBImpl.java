package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.Part;;
import com.dinos.vehiclerepairshop.utils.DBConnection;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PartDAODBImpl implements PartDAO {

    private DBConnection dbConnection = new DBConnection();

    private Part displayPart(ResultSet rs) throws SQLException {

        while (rs.next()) {
            long partId = rs.getLong("id");
            long repairId = rs.getLong("repair_id");
            String partType = rs.getString("part_name");
            double partCost = rs.getDouble("part_cost");

            Part part = new Part(partId, partType, partCost, repairId);
            return part;
        }
        return null;
    }

    @Override
    public void insertPart(Part p) {
        String sql = "insert into public.part(repair_id, part_name, part_cost)"
                + " values(?,?,?)";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setLong(1, p.getRepairId());
            prepSt.setString(2, p.getPartType());
            prepSt.setDouble(3, p.getPartCost());

            int newRow = prepSt.executeUpdate();
            if (newRow == 1) {
                System.out.println("Part created.");
            } else {
                System.out.println("Part already created.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean updatePart(Part partToUpdate) {
        String sql= "update public.part "+
                    "set repair_id = ?, part_name = ?, part_cost = ? "+
                    "where id = ?";

        int affectedRows;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setLong(1, partToUpdate.getRepairId());
            prepSt.setString(2, partToUpdate.getPartType());
            prepSt.setDouble(3, partToUpdate.getPartCost());
            prepSt.setInt(4, Math.toIntExact(partToUpdate.getPartId()));

            affectedRows = prepSt.executeUpdate();

            if (affectedRows != 0) {
                System.out.println("Part updated");
                return true;
            } else {
                System.out.println("Part not updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deletePartFromList(long partId) {
        String sql = "delete from vehicle where id = ?";
        int affectedRows = 0;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepStmnt = conn.prepareStatement(sql)) {
            prepStmnt.setLong(1, partId);
            affectedRows = prepStmnt.executeUpdate();

            if (affectedRows != 0) {
                System.out.println("Part deleted");
                return true;
            } else {
                System.out.println("Part not deleted or has already been deleted");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Part> findAllParts() {
        List<Part> parts = new ArrayList<>();
        String sql = "select * from public.part";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            ResultSet rs = prepSt.executeQuery();
            while (rs.next()) {
                long partId = rs.getLong("id");
                long repairId = rs.getLong("repair_id");
                String partType = rs.getString("part_name");
                double partCost = rs.getDouble("part_cost");

                Part part = new Part(partId, partType, partCost, repairId);
                parts.add(part);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return parts;
    }

    @Override
    public Part findPartById(long partId) {
        String sql = "select id, repair_id, part_name, part_cost"
                + " from public.part"
                + " where id = ?";
        try (Connection connection = dbConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, partId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return displayPart(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Part> findPartByType(String partType) {
        List<Part> partsSpecificType = new ArrayList<>();
        String sql = "select id, repair_id, part_name, part_cost"
                + " from public.part"
                + " where part_name = ?";
        try (Connection connection = dbConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, partType);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                long partId = rs.getLong("id");
                long repairId = rs.getLong("repair_id");
                double partCost = rs.getDouble("part_cost");

                Part part = new Part(partId, partType, partCost, repairId);
                partsSpecificType.add(part);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return partsSpecificType;
    }

    @Override
    public List<Part> findPartByCost(int partCost) {
        List<Part> partsSpecificCost = new ArrayList<>();
        String sql = "select id, repair_id, part_name, part_cost"
                + " from public.part"
                + " where part_cost = ?";
        try (Connection connection = dbConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setDouble(1, partCost);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                long partId = rs.getLong("id");
                long repairId = rs.getLong("repair_id");
                String partType = rs.getString("part_name");

                Part part = new Part(partId, partType, partCost, repairId);
                partsSpecificCost.add(part);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return partsSpecificCost;
    }

    @Override
    public List<Part> findRepairParts(long repairId) {
        List<Part> repairParts = new ArrayList<>();
        String sql = "select id, repair_id, part_name, part_cost"
                + " from public.part"
                + " where repair_id = ?";
        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            prepSt.setDouble(1, repairId);
            ResultSet rs = prepSt.executeQuery();

            while (rs.next()) {
                long partId = rs.getLong("id");
                String partType = rs.getString("part_name");
                double partCost = rs.getDouble("part_cost");

                Part part = new Part(partId, partType, partCost, repairId);
                repairParts.add(part);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return repairParts;
    }

    public double repairPartsCost(long repairId) {
        String sql = "select sum(part_cost) " +
                "as repair_parts_cost " +
                "from public.part " +
                "where repair_id = ?";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            prepSt.setDouble(1, repairId);
            ResultSet rs = prepSt.executeQuery();
            while (rs.next()) {
                double partsCost = rs.getDouble("repair_parts_cost");
                return partsCost;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Double.parseDouble(null);
    }
}
