package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.User;
import com.dinos.vehiclerepairshop.utils.DBConnection;
import com.dinos.vehiclerepairshop.utils.UserType;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAODBImpl implements UserDAO {

    private DBConnection dbConnection = new DBConnection();

    private User displayUser(ResultSet rs) throws SQLException {
        while (rs.next()) {
            long userId = rs.getInt("id");
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");
            String address = rs.getString("address");
            String email = rs.getString("email");
            String password = rs.getString("password_user");
            int afm = rs.getInt("afm");
            int userType = rs.getInt("user_type_id");

            User user = new User(userId, email, password, firstName, lastName, address, afm, intToType(userType));
            return user;
        }
        return null;
    }

    @Override
    public void insertUser(User user) {
        String sql = "insert into public.user(first_name, last_name, address, email, password_user, afm, user_type_id)"
                + "values(?,?,?,?,?,?,?)";
        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setString(1, user.getFirstName());
            prepSt.setString(2, user.getLastName());
            prepSt.setString(3, user.getAddress());
            prepSt.setString(4, user.getEmail());
            prepSt.setString(5, user.getPassword());
            prepSt.setLong(6, user.getAfm());
            prepSt.setInt(7, getTypeToInt(user));

            int newRow = prepSt.executeUpdate();
            if (newRow == 1)
                System.out.println("User " + user.getEmail() + " created.");


        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("User with email: "+ user.getEmail() +" exists. Try with another email.");
        }
    }


    private int getTypeToInt(User user) {
        if (user.getType() == UserType.USER) {
            return 2;
        } else
            return 1;
    }

    private UserType intToType(int key) {
        if (key == 1)
            return UserType.ADMINISTRATOR;
        else
            return UserType.USER;
    }


    @Override
    public boolean updateUser(User user) {
        String sql = "update public.user " +
                     "set first_name = ?, last_name = ?, address = ?, email = ?, password_user = ?, afm = ?, user_type_id = ? " +
                     "where id = ?";

        int affectedRows;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            prepSt.setString(1, user.getFirstName());
            prepSt.setString(2, user.getFirstName());
            prepSt.setString(3, user.getAddress());
            prepSt.setString(4, user.getEmail());
            prepSt.setString(5, user.getPassword());
            prepSt.setLong(6, user.getAfm());
            prepSt.setInt(7, getTypeToInt(user));
            prepSt.setInt(8, Math.toIntExact(user.getId()));

            affectedRows = prepSt.executeUpdate();
            if (affectedRows != 0) {
                System.out.println("User updated.");
                return true;
            } else {
                System.err.println("User not updated.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean deleteUser(long userId) {
        int affectedRows = 0;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement("delete from public.user where id = ?")) {
            prepSt.setLong(1, userId);
            affectedRows = prepSt.executeUpdate();
            if (affectedRows != 0) {
                System.out.println("User deleted");
                return true;
            } else {
                System.out.println("User not deleted or has already been deleted");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteUserByEmail(String email) {
        int affectedRows = 0;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement("delete from public.user where email = ?")) {
            prepSt.setString(1, email);
            affectedRows = prepSt.executeUpdate();
            if (affectedRows != 0) {
                System.out.println("User deleted");
                return true;
            } else {
                System.out.println("User not deleted or has already been deleted");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<User> findAllUsers() {
        List<User> users = new ArrayList<>();
        String sql = "select * from public.user";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            ResultSet rs = prepSt.executeQuery();
            while (rs.next()) {
                long userId = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String address = rs.getString("address");
                String email = rs.getString("email");
                String password = rs.getString("password_user");
                int afm = rs.getInt("afm");
                int userType = rs.getInt("user_type_id");

                User user = new User(userId, email, password, firstName, lastName, address, afm, intToType(userType));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User findUserById(long userId) {
        try (Connection connection = dbConnection.connect();
             PreparedStatement prepSt = connection.prepareStatement("select id, first_name, last_name, address, email, password_user, afm, user_type_id"
                     + " from public.user"
                     + " where id = ?")) {
            prepSt.setLong(1, userId);
            ResultSet resultSet = prepSt.executeQuery();
            return displayUser(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User findUserByEmail(String email) {

        try (Connection connection = dbConnection.connect();
             PreparedStatement prepSt = connection.prepareStatement("select id, first_name, last_name, address, email, password_user, afm, user_type_id"
                     + " from public.user"
                     + " where email = ?")) {
            prepSt.setString(1, email);
            ResultSet resultSet = prepSt.executeQuery();
            return displayUser(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean findUserByCredentials(String email, String password) {
        try (Connection connection = dbConnection.connect();
             PreparedStatement prepSt = connection.prepareStatement("select id, first_name, last_name, address, email, password_user, afm, user_type_id"
                     + " from public.user"
                     + " where email = ? and password = ?")) {
            prepSt.setString(1, email);
            prepSt.setString(2, password);

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
