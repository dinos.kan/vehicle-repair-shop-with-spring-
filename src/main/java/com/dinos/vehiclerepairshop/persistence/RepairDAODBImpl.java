package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.Part;
import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.utils.DBConnection;
import com.dinos.vehiclerepairshop.utils.RepairStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RepairDAODBImpl implements RepairDAO {

    private DBConnection dbConnection = new DBConnection();

    private Repair displayRepair(ResultSet rs) throws SQLException {
        while (rs.next()) {
            long repairId = rs.getLong("id");
            long vehicleId = rs.getLong("vehicle_id");
            LocalDate repairDate = LocalDate.parse(rs.getString("repair_date"));
            double repairCost = rs.getDouble("repair_cost");
            RepairStatus status = getIntToType(rs.getInt("repair_status_id"));

            Repair repair = new Repair(repairId, repairDate, status, vehicleId, repairCost);
            return repair;
        }
        return null;
    }

    @Override
    public void insertRepair(Repair r) {
        String sql = "insert into public.repair(vehicle_id, repair_date, repair_cost, repair_status_id)"
                + "values(?,?,?,?)";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setLong(1, r.getVehicleId());
            prepSt.setDate(2, Date.valueOf(r.getRepairDate()));
            prepSt.setDouble(3, r.getRepairCost());
            prepSt.setInt(4, getTypeToInt(r));

            int newRow = prepSt.executeUpdate();
            if (newRow == 1) {
                System.out.println("Repair created.");
            } else {
                System.out.println("Repair already created.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getTypeToInt(Repair repair) {
        if (repair.getRepairStatus() == RepairStatus.SCHEDULED)
            return 1;
        else if (repair.getRepairStatus() == RepairStatus.IN_PROGRESS)
            return 2;
        else if (repair.getRepairStatus() == RepairStatus.FINISHED)
            return 3;
        else return 4;
    }

    private RepairStatus getIntToType(int key) {
        if (key == 1) {
            return RepairStatus.SCHEDULED;
        } else if (key == 2) {
            return RepairStatus.IN_PROGRESS;
        } else if (key == 3) {
            return RepairStatus.FINISHED;
        } else
            return RepairStatus.CANCELED;
    }

    @Override
    public boolean updateRepair(Repair repairToUpdate) {
        String sql= "update public.repair " +
                    "set vehicle_id = ?, repair_date = ?, repair_cost =?, repair_status_id = ? " +
                    "where id = ?";

        int affectedRows;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setLong(1, repairToUpdate.getVehicleId());
            prepSt.setDate(2, Date.valueOf(repairToUpdate.getRepairDate()));
            prepSt.setDouble(3, repairToUpdate.getRepairCost());
            prepSt.setInt(4, getTypeToInt(repairToUpdate));

            affectedRows = prepSt.executeUpdate();

            if (affectedRows == 1) {
                System.out.println("Repair updated");
                return true;
            } else {
                System.out.println("Repair not updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteRepairFromList(long repairId) {

        String sql = "delete from vehicle where id = ?";
        int affectedRows = 0;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            prepSt.setLong(1, repairId);
            affectedRows = prepSt.executeUpdate();

            if (affectedRows != 0) {
                System.out.println("Repair deleted");
                return true;
            } else {
                System.out.println("Repair not deleted or has already been deleted");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Repair> findAllRepairs() {

        List<Repair> repairs = new ArrayList<>();
        String sql = "select * from public.repair";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            ResultSet rs = prepSt.executeQuery();
            while (rs.next()) {
                long repairId = rs.getLong("id");
                long vehicleId = rs.getLong("vehicle_id");
                LocalDate repairDate = LocalDate.parse(rs.getString("repair_date"));
                double repairCost = rs.getDouble("repair_cost");
                RepairStatus status = getIntToType(rs.getInt("repair_status_id"));

                Repair repair = new Repair(repairId, repairDate, status, vehicleId, repairCost);
                repairs.add(repair);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return repairs;
    }

    @Override
    public Repair findRepairById(long repairId) {
        try (Connection connection = dbConnection.connect();
             PreparedStatement prepSt = connection.prepareStatement("select id, vehicle_id, repair_date, repair_cost, repair_status_id"
                     + " from public.repair"
                     + " where id = ?")) {
            prepSt.setLong(1, repairId);
            ResultSet resultSet = prepSt.executeQuery();
            return displayRepair(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Repair> findVehicleRepairs(long vehicleId) {
        List<Repair> vehicleRepairs = new ArrayList<>();
        String sql = "select id, vehicle_id, repair_date, repair_cost, repair_status_id"
                + " from public.repair"
                + " where vehicle_id = ?";
        try (Connection connection = dbConnection.connect();
             PreparedStatement prepSt = connection.prepareStatement(sql)) {
            prepSt.setLong(1, vehicleId);
            ResultSet rs = prepSt.executeQuery();
            while (rs.next()) {
                long repairId = rs.getLong("id");
                LocalDate repairDate = LocalDate.parse(rs.getString("repair_date"));
                double repairCost = rs.getDouble("repair_cost");
                RepairStatus status = getIntToType(rs.getInt("repair_status_id"));

                Repair repair = new Repair(repairId, repairDate, status, vehicleId, repairCost);
                vehicleRepairs.add(repair);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vehicleRepairs;
    }

    @Override
    public double calcRepairCost(long repairId) {
        String sqlRepCost = "select repair_cost"
                + " from public.repair"
                + " where id = ?";

        try (Connection connection = dbConnection.connect();
             PreparedStatement prepSt = connection.prepareStatement(sqlRepCost)) {
            prepSt.setDouble(1, repairId);
            ResultSet rs = prepSt.executeQuery();
            while (rs.next()) {
                double repCost = rs.getDouble("repair_cost");
                return repCost;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Double.parseDouble(null);
    }
}
