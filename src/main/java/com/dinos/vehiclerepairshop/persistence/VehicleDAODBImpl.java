package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.Vehicle;
import com.dinos.vehiclerepairshop.utils.DBConnection;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleDAODBImpl implements VehicleDAO {

    private DBConnection dbConnection = new DBConnection();

    private Vehicle displayVehicle(ResultSet rs) throws SQLException {
        while (rs.next()) {
            long vehicleId = rs.getLong("id");
            long userId = rs.getLong("user_id");
            String plateNumber = rs.getString("plate_number");
            String brand = rs.getString("brand");
            String model = rs.getString("model");
            String color = rs.getString("color");
            LocalDate productionDate = LocalDate.parse(rs.getString("production_date"));
            int price = rs.getInt("price");

            Vehicle vehicle = new Vehicle(vehicleId, brand, model, productionDate, plateNumber, color, price, userId);
            return vehicle;
        }
        return null;
    }

    @Override
    public void insertVehicle(Vehicle v) {
        String sql = "insert into public.vehicle(user_id, plate_number, brand, model, color, production_date, price)"
                + "values(?,?,?,?,?,?,?)";

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setLong(1, v.getOwnerId());
            prepSt.setString(2, v.getPlateNumber());
            prepSt.setString(3, v.getBrand());
            prepSt.setString(4, v.getModel());
            prepSt.setString(5, v.getColor());
            prepSt.setDate(6, Date.valueOf(v.getCreationDate()));
            prepSt.setLong(7, v.getPrice());

            int newRow = prepSt.executeUpdate();
            if (newRow == 1) {
                System.out.println("Vehicle " + v.getBrand() + " created.");
            } else {
                System.out.println("Vehicle was not created.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean updateVehicle(Vehicle vehicleToUpdate) {
        String sql = "update public.vehicle " +
                     "set user_id = ?, plate_number = ?, brand = ?, model = ?, color = ?, production_date = ?, price = ? "+
                     "where id = ?";

        int affectedRows;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql,
                     Statement.RETURN_GENERATED_KEYS)) {

            prepSt.setLong(1, vehicleToUpdate.getOwnerId());
            prepSt.setString(2, vehicleToUpdate.getPlateNumber());
            prepSt.setString(3, vehicleToUpdate.getBrand());
            prepSt.setString(4, vehicleToUpdate.getModel());
            prepSt.setString(5, vehicleToUpdate.getColor());
            prepSt.setDate(6, Date.valueOf(vehicleToUpdate.getCreationDate()));
            prepSt.setLong(7, vehicleToUpdate.getPrice());
            prepSt.setInt(8, Math.toIntExact(vehicleToUpdate.getVehicleId()));

            affectedRows = prepSt.executeUpdate();

            if (affectedRows != 0) {
                System.out.println("Vehicle updated");
                return true;
            } else {
                System.out.println("Vehicle not updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteVehicleFromList(long id) {
        String sql = "delete from vehicle where id = ?";
        int affectedRows = 0;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            prepSt.setLong(1, id);
            affectedRows = prepSt.executeUpdate();

            if (affectedRows != 0) {
                System.out.println("Vehicle deleted");
                return true;
            } else {
                System.out.println("Vehicle not deleted or has already been deleted");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteVehiclesOfUser(long userId) {
        String sql = "delete from vehicle where user_id = ?";
        int affectedRows = 0;

        try (Connection conn = dbConnection.connect();
             PreparedStatement prepSt = conn.prepareStatement(sql)) {
            prepSt.setLong(1, userId);
            affectedRows = prepSt.executeUpdate();

            if (affectedRows != 0) {
                System.out.println("Vehicles deleted");
                return true;
            } else {
                System.out.println("Vehicles not deleted or have already been deleted");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Vehicle> findAllVehicles() {
        List<Vehicle> vehicles = new ArrayList<>();
        String sql = "select * from public.vehicle";

        try (Connection conn = dbConnection.connect();
             Statement prepSt = conn.createStatement()) {
            ResultSet rs = prepSt.executeQuery(sql);

            while (rs.next()) {
                long vehicleId = rs.getLong("id");
                long userId = rs.getLong("user_id");
                String plateNumber = rs.getString("plate_number");
                String brand = rs.getString("brand");
                String model = rs.getString("model");
                String color = rs.getString("color");
                LocalDate productionDate = LocalDate.parse(rs.getString("production_date"));
                int price = rs.getInt("price");

                Vehicle vehicle = new Vehicle(vehicleId, brand, model, productionDate, plateNumber, color, price, userId);
                vehicles.add(vehicle);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return vehicles;
    }

    @Override
    public Vehicle findVehicleById(long vehicleId) {
        String sql = "select id, user_id, plate_number, brand, model, color, production_date, price"
                + " from public.vehicle"
                + " where id = ?";
        try (Connection connection = dbConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, vehicleId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return displayVehicle(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Vehicle findVehicleByPlateNumber(String plateNumber) {
        String sql = "select id, user_id, plate_number, brand, model, color, production_date, price"
                + " from public.vehicle"
                + " where plate_number = ?";
        try (Connection connection = dbConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, plateNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            return displayVehicle(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Vehicle> findVehiclesByUserId(long userId) {
        String sql = "select id, user_id, plate_number, brand, model, color, production_date, price"
                + " from public.vehicle"
                + " where user_id = ?";
        List<Vehicle> userVehicles = new ArrayList<>();
        try (Connection connection = dbConnection.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, userId);
            ResultSet rs = preparedStatement.executeQuery();

            generateListOfVehicles(userVehicles, rs);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userVehicles;
    }

    private void generateListOfVehicles(List<Vehicle> userVehicles, ResultSet rs) throws SQLException {
        while (rs.next()) {
            long vehicleId = rs.getLong("id");
            long ownerId = rs.getLong("user_id");
            String plateNumber = rs.getString("plate_number");
            String brand = rs.getString("brand");
            String model = rs.getString("model");
            String color = rs.getString("color");
            LocalDate productionDate = LocalDate.parse(rs.getString("production_date"));
            int price = rs.getInt("price");

            Vehicle vehicle = new Vehicle(vehicleId, brand, model, productionDate, plateNumber, color, price, ownerId);
            userVehicles.add(vehicle);
        }
    }
}
