package com.dinos.vehiclerepairshop.persistence;

import com.dinos.vehiclerepairshop.domain.User;
import com.dinos.vehiclerepairshop.utils.UserType;

import java.util.List;

public interface UserDAO {
    void insertUser(User user);

    boolean deleteUser(long id);

    boolean updateUser(User userToUpdate);

    List<User> findAllUsers();

    User findUserById(long userId);

    User findUserByEmail(String givenData);

    boolean findUserByCredentials(String email, String password);
}
