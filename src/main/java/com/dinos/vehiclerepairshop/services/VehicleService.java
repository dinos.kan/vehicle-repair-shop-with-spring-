package com.dinos.vehiclerepairshop.services;

import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.domain.Vehicle;
import com.dinos.vehiclerepairshop.persistence.VehicleDAO;
import com.dinos.vehiclerepairshop.persistence.VehicleDAODBImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleService {

    @Autowired
    private VehicleDAO vehicleDAO;
    private RepairService repairService = new RepairService();

    public void insertVehicle(Vehicle vehicle){
        vehicleDAO.insertVehicle(vehicle);
    }

    public boolean removeVehicleFromList(long vehicleId){
        return vehicleDAO.deleteVehicleFromList(vehicleId);
    }

    public List<Vehicle> findAllVehicles(){
        return vehicleDAO.findAllVehicles();
    }

    public Vehicle findVehicleById(long vehicleId){
        if (vehicleDAO.findVehicleById(vehicleId) == null)
            System.out.println("Vehicle with given ID does not exist");
        return vehicleDAO.findVehicleById(vehicleId);
    }

    public Vehicle findVehicleByPlateNumber(String plateNumber){
        return vehicleDAO.findVehicleByPlateNumber(plateNumber);
    }

    public boolean updateVehicle(Vehicle newVehicle){
        return vehicleDAO.updateVehicle(newVehicle);
    }

    public List<Vehicle> findUserVehicles(long userId){
        return vehicleDAO.findVehiclesByUserId(userId);
    }

    public List<Repair> findUserRepairs(long userId){

        List<Vehicle> userVehicles = findUserVehicles(userId);
        List<Repair> userRepairs = new ArrayList<>();

        for (Vehicle vehicle:userVehicles){
            long vehicleId = vehicle.getVehicleId();
            userRepairs.addAll(repairService.findVehicleRepairs(vehicleId));
        }
        return userRepairs;
    }
}
