package com.dinos.vehiclerepairshop.services;

import com.dinos.vehiclerepairshop.domain.Part;;
import com.dinos.vehiclerepairshop.persistence.PartDAO;
import com.dinos.vehiclerepairshop.persistence.PartDAODBImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartService {

    @Autowired
    private PartDAO partDAODB;
    private static RepairService repairService = new RepairService();

    public void insertPart(Part part){
        partDAODB.insertPart(part);
    }

    public boolean removePartFromList(long partId){
        return partDAODB.deletePartFromList(partId);
    }

    public List<Part> findAllParts(){
       return partDAODB.findAllParts();
    }

    public Part findPartById(long partId){
        if (partDAODB.findPartById(partId) == null)
            System.out.println("Part with given ID does not exist");
        return partDAODB.findPartById(partId);
    }

    public List<Part> findPartByType(String partType){
        return partDAODB.findPartByType(partType);
    }

    public List<Part> findPartByCost(int partCost){
        return partDAODB.findPartByCost(partCost);
    }

    public boolean updatePart(Part newPart){
        return partDAODB.updatePart(newPart);
    }

    public List<Part> findRepairParts(long repairId){
        return partDAODB.findRepairParts(repairId);
    }

    public double repairPartsCost(long repairId){
        return partDAODB.repairPartsCost(repairId);
    }
}
