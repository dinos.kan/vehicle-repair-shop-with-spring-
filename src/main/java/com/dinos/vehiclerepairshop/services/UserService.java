package com.dinos.vehiclerepairshop.services;

import com.dinos.vehiclerepairshop.domain.User;
import com.dinos.vehiclerepairshop.persistence.UserDAODBImpl;
import com.dinos.vehiclerepairshop.persistence.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;
    private VehicleService vehicleService = new VehicleService();

    public void insertUser(User user) {
        userDAO.insertUser(user);
    }

    public boolean deleteUser(long id){
        return userDAO.deleteUser(id);
    }

    public List<User> findAllUsers(){
        return userDAO.findAllUsers();
    }

    public User findUserById(long userId) {
        if (userDAO.findUserById(userId) == null)
            System.out.println("User with given ID does not exist");
        return userDAO.findUserById(userId);
    }

    public User findUserByEmail(String givenData) {
        return userDAO.findUserByEmail(givenData);
    }

    public boolean findUserByCredentials(String email, String password) {
        return userDAO.findUserByCredentials(email, password);
    }

    public boolean updateUser(User newUser){
        return userDAO.updateUser(newUser);
    }

}
