package com.dinos.vehiclerepairshop.services;

import com.dinos.vehiclerepairshop.domain.Part;
import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.persistence.RepairDAODBImpl;
import com.dinos.vehiclerepairshop.utils.RepairStatus;
import com.dinos.vehiclerepairshop.persistence.RepairDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class RepairService {

    @Autowired
    private RepairDAO repairDAO;
    private PartService partService = new PartService();

    public void insertRepair(Repair repair){
        repairDAO.insertRepair(repair);
    }

    public boolean removeRepairFromList(long id){
        return repairDAO.deleteRepairFromList(id);
    }

    public List<Repair> findAllRepairs(){
        return repairDAO.findAllRepairs();
    }

    public Repair findRepairById(long repairId){
        if (repairDAO.findRepairById(repairId) == null)
            System.out.println("Repair with given ID does not exist");
        return repairDAO.findRepairById(repairId);
    }

    public List<Repair> findVehicleRepairs(long vehicleId){
        return repairDAO.findVehicleRepairs(vehicleId);
    }

    public boolean updateRepair(Repair newRepair){
        return repairDAO.updateRepair(newRepair);
    }

    public List<Part> findRepairParts(long repairId){
        return partService.findRepairParts(repairId);
    }

    public double calcRepairCost(long repairId){
        return repairDAO.calcRepairCost(repairId);
    }

    public double calcTotalCost(long repairId){
        double repairCost = calcRepairCost(repairId);
        double partsCost = partService.repairPartsCost(repairId);
        return repairCost + partsCost;
    }

}
