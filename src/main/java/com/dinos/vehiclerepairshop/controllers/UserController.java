package com.dinos.vehiclerepairshop.controllers;

import com.dinos.vehiclerepairshop.domain.User;
import com.dinos.vehiclerepairshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public void insert(@RequestBody User user){
        userService.insertUser(user);
    }

    @PutMapping
    public boolean update(@RequestBody User user){
        return userService.updateUser(user);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable(value = "id") long id){
        return userService.deleteUser(id);
    }

    @GetMapping
    public List<User> findAll(){
        return userService.findAllUsers();
    }

    @GetMapping(path = "/{id}")
    public User find(@PathVariable(value = "id") long id){
        return userService.findUserById(id);
    }

    @GetMapping(path = "/email/{email}")
    public User find(@PathVariable(value = "email") String email){
        return userService.findUserByEmail(email);
    }

}
