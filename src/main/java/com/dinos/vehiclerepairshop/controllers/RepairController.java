package com.dinos.vehiclerepairshop.controllers;

import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.domain.Vehicle;
import com.dinos.vehiclerepairshop.services.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/repairs")
public class RepairController {

    @Autowired
    private RepairService repairService;

    @PostMapping
    public void insert(@RequestBody Repair repair){
        repairService.insertRepair(repair);
    }

    @PutMapping
    public boolean update(@RequestBody Repair repair){
        return repairService.updateRepair(repair);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable(value = "id") long id){
        return repairService.removeRepairFromList(id);
    }

    @GetMapping
    public List<Repair> findAll(){
        return repairService.findAllRepairs();
    }

    @GetMapping(path = "/{id}")
    public Repair find(@PathVariable(value = "id") long id){
        return repairService.findRepairById(id);
    }

    @GetMapping(path = "/vehicle-repairs/{vehicle-id}")
    public List<Repair> findVehicleRepairs(@PathVariable(value = "vehicle-id") long vehicleId) {
        return repairService.findVehicleRepairs(vehicleId);
    }


}
