package com.dinos.vehiclerepairshop.controllers;

import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.domain.Vehicle;
import com.dinos.vehiclerepairshop.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Scanner;

@RestController
@RequestMapping(path = "/vehicles")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @PostMapping
    public void insert(@RequestBody Vehicle vehicle){
        vehicleService.insertVehicle(vehicle);
    }

    @PutMapping
    public boolean update(@RequestBody Vehicle vehicle){
        return vehicleService.updateVehicle(vehicle);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable(value = "id") long id){
        return vehicleService.removeVehicleFromList(id);
    }

    @GetMapping
    public List<Vehicle> findAll(){
        return vehicleService.findAllVehicles();
    }

    @GetMapping(path = "/id/{id}")
    public Vehicle find(@PathVariable(value = "id") long id){
        return vehicleService.findVehicleById(id);
    }

    @GetMapping(path = "/plate/{plate-number}")
    public Vehicle find(@PathVariable(value = "plate-number") String plateNumber){
        return vehicleService.findVehicleByPlateNumber(plateNumber);
    }

    @GetMapping(path = "/user-vehicles/{id}")
    public List<Vehicle> findUserVehicles(@PathVariable(value = "id") long id){
        return vehicleService.findUserVehicles(id);
    }

    @GetMapping(path = "/user-repairs/{id}")
    public List<Repair> findUserRepairs(@PathVariable(value = "id") long id){
        return vehicleService.findUserRepairs(id);
    }

}
