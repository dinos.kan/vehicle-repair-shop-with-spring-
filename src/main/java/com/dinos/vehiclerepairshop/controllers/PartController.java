package com.dinos.vehiclerepairshop.controllers;

import com.dinos.vehiclerepairshop.domain.Part;
import com.dinos.vehiclerepairshop.services.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Scanner;

@RestController
@RequestMapping(path = "/parts")
public class PartController {

    @Autowired
    private PartService partService;

    @PostMapping
    public void insert(@RequestBody Part part){
        partService.insertPart(part);
    }

    @PutMapping
    public boolean update(@RequestBody Part part){
        return partService.updatePart(part);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable(value = "id") long id){
        return partService.removePartFromList(id);
    }

    @GetMapping
    public List<Part> findAll(){
        return partService.findAllParts();
    }

    @GetMapping(path = "/{id}")
    public Part find(@PathVariable(value = "id") long id){
        return partService.findPartById(id);
    }

    @GetMapping(path = "/type/{type}")
    public List<Part> find(@PathVariable(value = "type") String type){
        return partService.findPartByType(type);
    }

    @GetMapping(path = "/cost/{cost}")
    public List<Part> findPartByCost(@PathVariable(value = "cost") int cost){
        return partService.findPartByCost(cost);
    }

    @GetMapping(path = "/repair-parts/{repair-id}")
    public List<Part> findRepairParts(@PathVariable(value = "repair-id") long repairId){
        return partService.findRepairParts(repairId);
    }


}
