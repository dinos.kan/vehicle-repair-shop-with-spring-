package com.dinos.vehiclerepairshop;

import com.dinos.vehiclerepairshop.controllers.PartController;
import com.dinos.vehiclerepairshop.controllers.RepairController;
import com.dinos.vehiclerepairshop.controllers.VehicleController;
import com.dinos.vehiclerepairshop.domain.Part;
import com.dinos.vehiclerepairshop.domain.Repair;
import com.dinos.vehiclerepairshop.domain.User;
import com.dinos.vehiclerepairshop.domain.Vehicle;
import com.dinos.vehiclerepairshop.persistence.UserDAODBImpl;
import com.dinos.vehiclerepairshop.services.PartService;
import com.dinos.vehiclerepairshop.services.RepairService;
import com.dinos.vehiclerepairshop.services.UserService;
import com.dinos.vehiclerepairshop.services.VehicleService;
import com.dinos.vehiclerepairshop.utils.DBConnection;
import com.dinos.vehiclerepairshop.utils.UserType;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import static com.dinos.vehiclerepairshop.utils.RepairStatus.FINISHED;

public class Main {


    public static void main(String[] args) {

        UserService userService = new UserService();
        VehicleService vehicleService = new VehicleService();
        RepairService repairService = new RepairService();
        PartService partService = new PartService();

        LocalDate date4 = LocalDate.of(2018, 11, 17);

        Vehicle vehicle = new Vehicle(4, "BMW", "Z4", date4 , "ABC1234", "white", 28900, 1);
        Repair repair = new Repair(10, date4, FINISHED, 1, 60);
        Part part = new Part(10, "lebgies", 97, 3);

//        vehicleService.insertVehicle(vehicle);
//        repairService.insertRepair(repair);
//        partService.insertPart(part);
//        List<Vehicle> allVehicles = vehicleService.findAllVehicles();
//        System.out.println(allVehicles);

//        //DATES
//        LocalDate date1 = LocalDate.of(2015, 12, 20);
//        LocalDate date2 = LocalDate.of(2010, 10, 10);
//        LocalDate date3 = LocalDate.of(2005, 8, 15);
//        LocalDate date5 = LocalDate.of(1999, 4, 13);
//        LocalDate date6 = LocalDate.of(2007, 6, 15);
//        LocalDate date7 = LocalDate.of(2006, 8, 17);
//        LocalDate date8 = LocalDate.of(2011, 10, 19);
//        LocalDate date9 = LocalDate.of(2014, 1, 21);
//        LocalDate date10 = LocalDate.of(2015, 10, 5);
//
//
//        //VEHICLES
//        vehicleService.insertVehicle(1, "Alfa Romeo", "156", "ZZZ9008", date1, "white", 5000, 17111993);
//        vehicleService.insertVehicle(2, "Audi", "A1", "KMT1718", date2, "red", 10500, 15021975);
//        vehicleService.insertVehicle(3, "Audi", "TT", "ZKN7890", date3, "black", 15000, 30091993);
//        vehicleService.insertVehicle(4, "BMW", "Z4", "KAM1711", date4, "white", 28900, 17111993);
//
//
//        //USERS
//        userService.insertUser(1001, "takis@takis.com", "takis", "Panagiotis", "Tokos",
//                "Egaleo", 123456789,SIMPLE_USER);
//        userService.insertUser(1002, "dinos@dinos.com", "dinos", "Konstantinos", "Kanakakis",
//                "Egaleo", 160010264, SIMPLE_USER);
//        userService.insertUser(1003, "makis@makis.com", "makis", "Prodromos", "Sakis",
//                "Peristeri", 987654321, ADMINISTRATOR);
//        userService.insertUser(1004, "nana@nana.com", "nana", "Anastasia", "Kanakaki",
//                "Chaidari", 567891234, UserType.ADMINISTRATOR);
        User userToUpdate = new User(16578, "dindxhfgcvkk@os", "takis", "Panagiotis", "Tsoukalas",
                "Pireas", 123456789, UserType.ADMINISTRATOR);
        userService.insertUser(userToUpdate);
//

//        List<User> allUsers = userDAODB.findAllUsers();
//        System.out.println(allUsers);
//        //REPAIRS
//        repairService.insertRepair(10, date5, FINISHED, 1, 60);
//        repairService.insertRepair(11, date6, IN_PROGRESS, 2, 30.2);
//        repairService.insertRepair(12, date6, SCHEDULED, 1, 25.7);
//        repairService.insertRepair(13, date7, IN_PROGRESS, 3, 15);
//
//
//        //PARTS
//        partService.insertPart(91,"engine", 300, 10);
//        partService.insertPart(92, "wheel", 90.5, 10);
//        partService.insertPart(93, "sensors", 45.4, 11);
//        partService.insertPart(94, "airbag", 65,13);


        //Methods calling
        List<User> allUsers = userService.findAllUsers();
        System.out.println(allUsers);
//
//        double cost = partService.repairPartsCost(1);
//        System.out.println(cost);

//        double costTotal = repairService.calcTotalCost(1);
//        System.out.println(costTotal);
//
//        List<Vehicle> allVehicles = vehicleService.findUserVehicles(1);
//        System.out.println(allVehicles);
//
//        List<Repair> allRepairs = repairService.findAllRepairs();
//        System.out.println(allRepairs);
//
//        List<Part> allParts = partService.findAllParts();
//        System.out.println(allParts);
//
//        boolean b = userService.deleteUserFromList(1004);
//        System.out.println(b);
//
//        boolean bb = userService.updateUser(userToUpdate);
//        System.out.println(bb);
//
//        List<User> allUsersUpdated = userService.findAllUsers();
//        System.out.println(allUsersUpdated);
//
//        List<User> users = userService.findUserByData("Konstantinos");
//        System.out.println(users.toString());
//
//        User user = userService.findUserById(1001);
//        System.out.println(user);
//
//        Repair repair = repairService.findRepairById(10);
//        System.out.println(repair);
//
//        Vehicle vehicle = vehicleService.findVehicleById(1);
//        System.out.println(vehicle);
//
//        List<Vehicle> vehicle1 = vehicleService.findVehicleByPlateNumber("white");
//        System.out.println(vehicle1.toString());
//
//        List<Repair> vehicleRepairs = repairService.findVehicleRepairs(1);
//        System.out.println(vehicleRepairs.toString());
//
//        List<Repair> userRepairs = vehicleService.findUserRepairs(17111993);
//        System.out.println(userRepairs.toString());
//
//        List<Part> parts = partService.findRepairParts(10);
//        System.out.println(parts.toString());
//
//        List<Part> parts1 = repairService.findRepairParts(10);
//        System.out.println(parts1.toString());
//
//        List<Vehicle> vehicles = vehicleService.findUserVehicles(30091993);
//        System.out.println(vehicles);
//
//        double totalCost = repairService.calcTotalCost(10);
//        System.out.println(totalCost);

    }
}














